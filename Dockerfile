FROM docker.io/library/ubuntu

COPY setup.bash .
RUN ./setup.bash

COPY Gemfile Gemfile.lock ./
RUN bundle install

CMD ["bundle", "exec", "jekyll", "serve", "--host", "0.0.0.0"]
