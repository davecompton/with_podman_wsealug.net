import { parse, Component } from "ical.js"

class Event {
  when: Date;
  where: string;
  what: string;
  description: string;
  url: string;

  constructor(when: Date, where: string, what: string, description: string, url: string) {
    this.when = when;
    this.where = where;
    this.what = what;
    this.description = description;
    this.url = url;
  }

}

// generate (accumulate) a string of HTML for a single event .
function generateEventHtml(ins: string, ev : Event) : string {
  const options : Intl.DateTimeFormatOptions = { timeZone: 'America/Los_Angeles',  
                                                 weekday:  'long',
                                                 month:    'long', 
                                                 day:      'numeric', 
                                                 hour:     'numeric', 
                                                 minute:   'numeric',
                                               };

  const when = ev.when.toLocaleString('en-US', options);
                     ins += "<br>";
                     ins += "<h5 >" + ev.what + "</h5>";
  if(ev.url)         ins += "<a href=\"" + ev.url + "\" target=\"_blank\">";
                     ins +=   "<ul>";
                     ins +=     "<li>" + when        + "</li>";
                     ins +=     "<li>" + ev.where       + "</li>";
  if(ev.description) ins +=     "<li>" + ev.description + "</li>";
  if(ev.url)         ins +=     "<li>" + ev.url         + "</li>";
                     ins +=   "</ul>";
  if(ev.url)         ins += "</a>";
  return ins;
}

// Get an ics calendar from url. 
// convert  to html and pass it on to callback.
async function fetchEvents(icsUrl : string) {
    const response = await fetch(icsUrl);
    const event = await response.text();
    const jcalData = parse(event);
    const vcalendar = new Component(jcalData);
    const vevents = vcalendar.getAllSubcomponents('vevent');
    const evs = vevents.map(ev => { const dstart = ev.getFirstPropertyValue('dtstart');
                                    return new Event( new Date(Date.parse(dstart)), 
                                                      ev.getFirstPropertyValue('location'),
                                                      ev.getFirstPropertyValue('summary'),
                                                      ev.getFirstPropertyValue('description'),
                                                      ev.getFirstPropertyValue('url') ); })
    return evs;
}

// Thank you, @Toastrackenigma : https://stackoverflow.com/a/30280636/509928
function isDST(d) {
    let jan = new Date(d.getFullYear(), 0, 1).getTimezoneOffset();
    let jul = new Date(d.getFullYear(), 6, 1).getTimezoneOffset();
    return Math.max(jan, jul) !== d.getTimezoneOffset();    
}

function generateEvents() : Event[] {
    const ds0 = "26 Feb 2022 9:00:00 PST"; // known meeting start sometime in past.
    const d0 = new Date(Date.parse(ds0));
    const d0Ms = d0.getTime();

    const oneHourOfMs = 1000 * 60 * 60;
    const twoWeeksOfMs = oneHourOfMs * 24 * 7 * 2;


    // Tested by trying out different dates. 
    // const nowDate = new Date(Date.parse(    "26 Aug 2022 10:00:00 PDT" ));
    // const nowDate = new Date(Date.parse(    "26 Feb 2022 22:00:00 PDT" ));
    // const nowDate = new Date(Date.parse(    "18 Mar 2022 2:00:00 PDT"  ));
    // const nowDate = new Date(Date.parse(    "18 Jul 2025 12:00:00 PDT" ));
    // const nowMs = nowDate.getTime();

    // Also tested by trying out by setting timezone to east coast on computer running browser.

    const nowMs = Date.now();
    const deltaMs = Math.ceil((nowMs - d0Ms) / twoWeeksOfMs) * twoWeeksOfMs;
    const nextDateMs = d0Ms + deltaMs;

    let dNext = new Date();
    dNext.setTime(nextDateMs);

    if (isDST(dNext)) {
		dNext.setTime(nextDateMs - oneHourOfMs);  // adjust for setting clock forward in spring
    }
    
    const dates = [ dNext ];

    return dates.map(d => new Event( d
                                   , "Jitsi Virtual Meeting"
                                   , "Bi-Weekly Meeting"
                                   , undefined
                                   , undefined ));
}

async function fetchEventsWithAlternate(icsUrl : string) {
    try {
        const evs = await fetchEvents(icsUrl);
        return evs;
    } catch {
        const evs = generateEvents();
        return evs;
    }
}


// Get an ics calendar from url. 
// convert  to html and pass it on to callback.
async function getEvent(icsUrl : string, setHtmlCallbackback : (html:string) => any) {
  try {
    const evs = await fetchEventsWithAlternate(icsUrl);
    const ins = evs.sort((a,b) => a.when.getTime() - b.when.getTime())
                   .reduce(generateEventHtml, "<h4>Upcoming Events:</h4>");
    setHtmlCallbackback (ins);
  } catch (err) {
    setHtmlCallbackback(err);
  }
}

// per: https://stackoverflow.com/questions/23296094/browserify-how-to-call-function-bundled-in-a-file-generated-through-browserify
// and: https://stackoverflow.com/questions/12709074/how-do-you-explicitly-set-a-new-property-on-window-in-typescript

(<any>window).getEvent = getEvent;
