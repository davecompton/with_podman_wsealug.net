#! /bin/bash

apt-get update 
apt-get install -y --no-install-recommends \
  g++ \
  make \
  ruby-dev \
  npm 

gem install bundler

